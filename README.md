# README #

https://nitishtest.herokuapp.com/

This application is used to create a TODO in a spcified TODO List on basecamp with an attachment.

Access Tokens are prone to expire, so added an optional utility to generate access token when using application. Get the verification code using the link and copy the code in GET param of redirected URL to get access token.

If access token is provided in the form that access token will take priority over the access token set in the settings file.

Please contact tanu.kansal@gmail.com if needs any clarification.