import json
import requests

from django.shortcuts import render
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.conf import settings
from django.shortcuts import render

from basecamp.forms import ConnectBasecampForm, BasecampAccessTokenForm
from basecamp.basecampapi import ConnectBasecamp


def connect_basecamp(request):
    context = {
        'form': ConnectBasecampForm()
    }

    if request.method == 'POST':
        form = ConnectBasecampForm(request.POST, request.FILES)

        if form.is_valid():
            if request.POST.get('access_token', ''):
                access_token = request.POST.get('access_token', '')
            else:
                access_token = settings.BASECAMP_ACCESS_TOKEN

            basecamp_obj = ConnectBasecamp(access_token)
            response = basecamp_obj.create_todo(request.POST.get('title', ''), request.FILES.get('attachment', ''))

            if response.status_code == 201:
                context.update({'success': True})
            else:
                context.update({'failure': True})

    return render(request, 'connect_basecamp.html', context)


def get_verification_code(request):
    code_url = "%s?client_id=%s&redirect_uri=%s&type=%s" % (
        settings.BASECAMP_AUTHORIZATION_URI,
        settings.BASECAMP_CLIENT_ID,
        settings.BASECAMP_REDIRECT_URI,
        settings.BASECAMP_TYPE
    )

    return HttpResponseRedirect(code_url)


def get_access_token(request):
    context = {
        'form': BasecampAccessTokenForm()
    }

    if request.method == 'POST':
        form = BasecampAccessTokenForm(request.POST)

        if form.is_valid():
            url = "%s?code=%s&client_id=%s&client_secret=%s&redirect_uri=%s&type=%s" % (
                settings.BASECAMP_ACCESS_TOKEN_URI,
                request.POST['code'],
                settings.BASECAMP_CLIENT_ID,
                settings.BASECAMP_CLIENT_SECRET,
                settings.BASECAMP_REDIRECT_URI,
                settings.BASECAMP_TYPE
            )

            response = requests.post(url)
            data = json.loads(response.text)
            if response.status_code == 200:
                access_token = data['access_token']
                context.update({'access_token': access_token})
            else:
                api_error = data
                context.update({'api_error': api_error})
        else:
            context['form'] = form

    return render(request, 'get_access_token.html', context)
