from django import forms


class ConnectBasecampForm(forms.Form):
    access_token = forms.CharField(widget=forms.TextInput(attrs={'class':'input-text','placeholder':'Access Token(Optional)'}), required=False, max_length=500)
    title = forms.CharField(widget=forms.TextInput(attrs={'class':'input-text','placeholder':'Title'}), max_length=300)
    attachment = forms.FileField()


class BasecampAccessTokenForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={'class':'input-text','placeholder':'Verification Code'}), max_length=30)
