import json
import requests

from django.conf import settings


class ConnectBasecamp(object):
    
    def __init__(self, access_token):
        self.headers = {
            "Authorization": "Bearer %s" % access_token
        }

    def create_attachment(self, attachment_file):
        filename = attachment_file.name
        
        api_url = "/attachments.json?name=%s" % filename

        self.headers.update({
            'Content-Type': attachment_file.content_type,
            'Content-Length': str(len(attachment_file))
        })

        files = {'file': attachment_file.read()}
        response = requests.post('%s%s' % (settings.BASECAMP_API_BASE_URL, api_url), headers=self.headers, files=files)

        data = json.loads(response.text)
        return data['attachable_sgid']

    def create_todo(self, title, attachment_file):
        attachment_sgid = self.create_attachment(attachment_file)

        api_url = "/buckets/%s/todolists/%s/todos.json" % (settings.BASECAMP_PROJECT_ID, settings.BASECAMP_TODOLIST_ID)

        self.headers.update({
            'Content-Type': 'application/json'
        })

        params = {
            "content": title,
            "description": "<bc-attachment sgid='%s'></bc-attachment>" % attachment_sgid
        }

        response = requests.post('%s%s' % (settings.BASECAMP_API_BASE_URL, api_url), data=json.dumps(params), headers=self.headers)

        return response
