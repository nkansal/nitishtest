django==1.10
requests==2.18.4
gunicorn==19.7.1
dj-database-url==0.4.2
psycopg2==2.7.3.1
whitenoise==3.3.1
